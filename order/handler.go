package order

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/sway-commerce/sway-commerce-api/auth"
	"bitbucket.org/sway-commerce/sway-commerce-api/cart"
	"bitbucket.org/sway-commerce/sway-commerce-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-api/email"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-api/payment_methods"
	"bitbucket.org/sway-commerce/sway-commerce-api/total"
	"bitbucket.org/sway-commerce/sway-commerce-api/user"
	log "github.com/sirupsen/logrus"
)

func PlaceOrder(w http.ResponseWriter, r *http.Request) {
	var (
		orderData        PlaceOrderData
		customerData     user.CustomerData
		userId           int
		userIdInt64      int64
		orderTotals      total.Totals
		addressForTotals total.AddressData
	)
	err := json.NewDecoder(r.Body).Decode(&orderData)
	helpers.PanicErr(err)
	fmt.Print(orderData)

	// Gets user cart from MySQL by userId
	cartFromSQL := cart.GetCartFromMySql(orderData.CartId)

	// Does check if items send with request match items in user cart
	err = CheckIfItemsMatchInCart(cartFromSQL, orderData)
	helpers.PanicErr(err)

	// Performs stock check
	// err = CheckStockStatus(orderData)
	// helpers.PanicErr(err)

	// Performs FinalPrice check between items send and items in SSOT
	err = FinalPriceCheck(orderData)
	helpers.PanicErr(err)

	// Get customer data from MySql by id send in request
	if len(orderData.UserId) > 0 {
		userId, err = strconv.Atoi(orderData.UserId)
		helpers.PanicErr(err)
		userIdInt64 = int64(userId)
		customerData = user.GetUserFromMySQLById(userIdInt64)
	} else {
		customerData = user.CustomerData{
			Email:     orderData.PersonalData.Email,
			FirstName: orderData.PersonalData.Firstname,
			LastName:  orderData.PersonalData.Lastname,
			GroupID:   1}
		userId = 0
		userIdInt64 = 0
	}

	// Saves billing addresses to MySQL
	billingAddress := AssignDataToBillingAddressAndSaveIt(orderData)

	// Address saved to DB
	billingAddress.InsertOrUpdateAddressIntoMySQL(userIdInt64)

	// Calculates order totals using cart from MongoDB

	addressForTotals.AddressInformation.ShippingCarrierCode = orderData.AddressInformation.ShippingCarrierCode
	addressForTotals.AddressInformation.ShippingMethodCode = orderData.AddressInformation.ShippingMethodCode
	orderTotals.CalculateTotals(orderData.CartId, addressForTotals, customerData.GroupID)

	// Preparing order history
	orderHistory := FormatOrderHistory(orderTotals, customerData, billingAddress.ID, cartFromSQL.QuoteId)

	// Working on Items in Order history []items
	orderItems := FormatOrderHistoryItems(orderTotals, cartFromSQL.QuoteId)
	orderHistory.Items = orderItems

	// Working on Payment information
	paymentMethod := payment_methods.GetPaymentMethodFromDbByMethodCode(orderData.AddressInformation.PaymentMethodCode)
	orderPayment := FormatPaymentData(orderHistory, paymentMethod, cartFromSQL.QuoteId, userId)

	// Shipping assignment
	shippingAssignment := AssignDataToShippingAssignmentsAndSaveIt(orderData, orderHistory, orderTotals)

	// Saves order to MySQL
	orderHistory.SaveOrder()

	// Assigns OrderId to order items
	// Saves them to MySQL
	for i := 0; i < len(orderHistory.Items); i++ {
		orderHistory.Items[i].OrderId = orderHistory.ID
		orderHistory.Items[i].SaveItem()
	}

	// Saves payment_methods data to MySQL
	orderPayment.OrderId = orderHistory.ID
	orderPayment.SavePaymentData(orderHistory.ID)

	// Saves shipping_methods address to MySQL
	shippingAssignment.Shipping.Address.SaveOrderShippingAddress(orderHistory.ID)

	//Checks if payment is cash on delivery
	if orderPayment.Method == "cashondelivery" {
		fmt.Println("@@@Insert functionality for cash on delivery here")
	}

	// Assign data to order history
	orderHistory.BillingAddress = BillingAddress(
		Address{
			Id:          billingAddress.ID,
			CustomerId:  billingAddress.CustomerID,
			City:        billingAddress.City,
			Company:     billingAddress.Company,
			CountryId:   billingAddress.CountryID,
			Email:       billingAddress.Email,
			Firstname:   billingAddress.Firstname,
			Lastname:    billingAddress.Lastname,
			Postcode:    billingAddress.Postcode,
			Region:      billingAddress.Region.Region,
			RegionCode:  billingAddress.Region.RegionCode,
			RegionId:    billingAddress.Region.RegionID,
			Street:      billingAddress.Street,
			StreetLine0: billingAddress.StreetLine0,
			StreetLine1: billingAddress.StreetLine1,
			Telephone:   billingAddress.Telephone,
			OrderId:     orderHistory.ID})
	orderHistory.Payment = orderPayment
	shippingAssignment.Items = orderHistory.Items
	orderHistory.ExtensionAttributes.ShippingAssignments = append(orderHistory.ExtensionAttributes.ShippingAssignments, shippingAssignment)

	// Writes order to json file
	//orderHistory.WriteToJsonFile()

	// Order Pickup
	//@@@
	//orderPickup := orderHistory.BuildOrderPickupForm()
	//orderPickup.MakeOrderPickup()

	// Changes cart status to "Inactive" in MySQL
	defer cart.UpdateCartStatusInMySQL(cartFromSQL.QuoteId)

	integrateAmoCrm()

	resp := map[string]int{
		"code": 200}

	helpers.WriteResultWithStatusCode(w, resp, http.StatusOK)
}

func integrateAmoCrm() {
	emailConf := config.Conf.Email
	body := "Test email after order was placed."

	to := emailConf.From
	subject := "Test email after order was placed"

	err := email.SendMail(body, to, subject)
	if err != nil {
		log.Errorf("error on sending mail after orderPost %v", err)
	}
}

func GetCustomerOrderHistory(w http.ResponseWriter, r *http.Request) {
	urlToken, err := helpers.GetTokenFromUrl(r)
	helpers.PanicErr(err)
	token := auth.ParseToken(urlToken)
	claims, err := auth.GetTokenClaims(token)
	helpers.CheckErr(err)
	if err != nil {
		helpers.WriteResultWithStatusCode(w, err, http.StatusBadRequest)
	} else {
		subInt, err := strconv.Atoi(claims["sub"].(string))
		helpers.PanicErr(err)
		orderHistory := GetAllCustomerOrderHistory(subInt)

		result := result{
			Items:      orderHistory,
			TotalCount: len(orderHistory),
		}
		response := response{
			Code:   http.StatusOK,
			Result: result,
		}
		helpers.WriteResultWithStatusCode(w, response, response.Code)

	}
}
