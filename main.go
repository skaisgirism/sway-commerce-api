package main

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/cart"
	"bitbucket.org/sway-commerce/sway-commerce-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-api/currency"
	"bitbucket.org/sway-commerce/sway-commerce-api/discount"
	"bitbucket.org/sway-commerce/sway-commerce-api/email"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-api/language"
	"bitbucket.org/sway-commerce/sway-commerce-api/middleware"
	"bitbucket.org/sway-commerce/sway-commerce-api/order"
	"bitbucket.org/sway-commerce/sway-commerce-api/payment/klarna"
	"bitbucket.org/sway-commerce/sway-commerce-api/payment/paypal"
	"bitbucket.org/sway-commerce/sway-commerce-api/payment/paysera"
	"bitbucket.org/sway-commerce/sway-commerce-api/payment_methods"
	"bitbucket.org/sway-commerce/sway-commerce-api/product"
	"bitbucket.org/sway-commerce/sway-commerce-api/serve"
	"bitbucket.org/sway-commerce/sway-commerce-api/shipping/postNord"
	"bitbucket.org/sway-commerce/sway-commerce-api/shipping_methods"
	"bitbucket.org/sway-commerce/sway-commerce-api/stock"
	"bitbucket.org/sway-commerce/sway-commerce-api/todoMongo"
	"bitbucket.org/sway-commerce/sway-commerce-api/total"
	"bitbucket.org/sway-commerce/sway-commerce-api/user"
	"github.com/go-chi/chi"
	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func init() {
	config.GetConfig("config.yml")
}

var DefaultLogger func(next http.Handler) http.Handler

func main() {

	klarna.GetClient()
	r := chi.NewRouter()
	//
	//DefaultLogger = m.RequestLogger(&m.DefaultLogFormatter{Logger: logs.New(config.Conf.Log.Out, "", logs.LstdFlags), NoColor: true})
	//
	//r.Use(DefaultLogger)
	//
	//r.Use(m.Logger)
	r.Mount("/", serve.InitRouter())
	r.Mount("/api/user", middleware.Log(user.RouterUser()))
	r.Mount("/api/cart", cart.RouterCart())
	r.Mount("/api/currency", currency.RouterCurrency())
	r.Mount("/api/language", language.RouterLanguage())
	r.Mount("/api/todo", todoMongo.TodoRouter())
	r.Mount("/api/stock", stock.RouterStock())
	r.Mount("/api/payment-methods", payment_methods.RouterPayment())
	r.Mount("/api/shipping-methods", shipping_methods.RoutesShippingMethods())
	r.Mount("/api/totals", total.RoutesTotal())
	r.Mount("/api/order", order.RouterOrder())
	r.Mount("/api/mail", email.InitRouter())
	r.Mount("/api/product", product.RouterProduct())
	r.Mount("/api/discount", discount.RouterDiscount())
	r.Mount("/api/cart/apply-cartcoupon", discount.CouponRouter())
	r.Mount("/api/payment/paypal", paypal.RoutesPaypal())
	r.Mount("/api/payment/klarna", klarna.RouterKlarna())
	r.Mount("/api/payment/paysera", paysera.RoutesPaysera())
	r.Mount("/api/payment/cashondelivery", paysera.RoutesPaysera())
	r.Mount("/api/postnord", postNord.RouterPostNord())

	log.Println("---------------------------------------------------------------------------")
	log.Println("--")
	log.Println("--  Started sway-commerce-api on " + config.Conf.Port)
	log.Println("--")
	log.Println("---------------------------------------------------------------------------")

	err := http.ListenAndServe(config.Conf.Port, r)
	helpers.PanicErr(err)
}
