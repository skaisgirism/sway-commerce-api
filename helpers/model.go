package helpers

const (
	HeaderContentType       = "Content-Type"
	HeaderAccept            = "Accept"
	MIMEApplicationJSON     = "application/json"
	MIMEApplicationJSONUtf8 = "application/json; charset=utf-8"
)

type (
	Response struct {
		Code   int         `json:"code,omitempty"`
		Result interface{} `json:"result,omitempty"`
		Meta   interface{} `json:"meta,omitempty"`
		Error  error
	}
	Closer interface {
		Close() error
	}
)
