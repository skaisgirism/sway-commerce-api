package cart

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/payment_methods"
	"bitbucket.org/sway-commerce/sway-commerce-api/shipping_methods"
	"github.com/go-chi/chi"
	"net/http"
)

func RouterCart() http.Handler {
	r := chi.NewRouter()
	r.Post("/create", createCart)
	r.Get("/pull", pullCart)
	r.Post("/update", updateCart)
	r.Post("/delete", deleteFromUserCart)
	r.Post("/payment-methods", payment_methods.AddPaymentMethods)
	r.Get("/payment-methods", payment_methods.GetPaymentMethods)
	r.Post("/shipping-methods", shipping_methods.GetShippingMethods)
	return r
}
