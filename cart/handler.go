package cart

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/sway-commerce/sway-commerce-api/attribute"
	"bitbucket.org/sway-commerce/sway-commerce-api/auth"
	"bitbucket.org/sway-commerce/sway-commerce-api/counter"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-api/product"
)

func createCart(w http.ResponseWriter, r *http.Request) {
	urlToken, _ := helpers.GetTokenFromUrl(r)

	if len(urlToken) > 0 {
		token := auth.ParseToken(urlToken)
		claims, err := auth.GetTokenClaims(token)
		helpers.CheckErr(err)

		if err != nil {
			helpers.WriteResultWithStatusCode(w, err, http.StatusForbidden)
		} else {
			if auth.CheckIfTokenIsNotExpired(claims) {
				cartID, err := CheckIfUserHasACart(claims["sub"].(string))

				//cartID, err := CheckDoesUserHasACartMongo(claims["sub"].(string))
				if err != nil {
					fmt.Println(err)
					cartID = CreateCartInMySQL(claims["sub"].(string))
				}
				response := helpers.Response{
					Code:   http.StatusOK,
					Result: cartID}
				response.SendResponse(w)
			} else {
				helpers.WriteResultWithStatusCode(w, "Token is expired", http.StatusForbidden)
			}
		}
	} else {
		// ToDo After placing order request to createCart comes in without token in url
		cartID := CreateCartInMySQL("")
		// cartID := CreateCartInMySQL("")
		response := helpers.Response{
			Code:   http.StatusOK,
			Result: cartID}
		response.SendResponse(w)
	}
}

func pullCart(w http.ResponseWriter, r *http.Request) {
	urlUserToken, _ := helpers.GetTokenFromUrl(r)
	urlCartId, err := helpers.GetCartIdFromUrl(r)
	helpers.CheckErr(err)

	if len(urlUserToken) > 0 {
		token := auth.ParseToken(urlUserToken)
		claims, err := auth.GetTokenClaims(token)
		helpers.CheckErr(err)

		if err != nil {
			helpers.WriteResultWithStatusCode(w, err, http.StatusForbidden)
		} else {
			if auth.CheckIfTokenIsNotExpired(claims) {
				cart := GetCartFromMySql(urlCartId)
				response := helpers.Response{
					Code:   http.StatusOK,
					Result: cart.Items}
				response.SendResponse(w)
			} else {
				helpers.WriteResultWithStatusCode(w, "Token is expired", http.StatusForbidden)
			}
		}
	} else {
		cart := GetCartFromMySql(urlCartId)
		response := helpers.Response{
			Code:   http.StatusOK,
			Result: cart.Items}
		response.SendResponse(w)
	}
}

func updateCart(w http.ResponseWriter, r *http.Request) {
	urlCartId, err := helpers.GetCartIdFromUrl(r)
	helpers.CheckErr(err)

	var item CustomerCart
	err = json.NewDecoder(r.Body).Decode(&item)
	helpers.PanicErr(err)
	var attributes []attribute.ItemAttribute
	for _, itemOptions := range item.Item.ProductOption.ExtensionAttributes.ConfigurableItemOptions {
		attributes = append(attributes, attribute.GetAttributeNameFromSolr(itemOptions.OptionsID, itemOptions.OptionValue))
	}

	productFromSolr := product.GetProductFromSolrBySKU(item.Item.SKU)
	if len(item.Item.SKU) == 4 {
		item.Item.SKU = product.BuildSKUFromItemAttributes(attributes, item.Item.SKU)
	}
	item.Item.Price = productFromSolr.Price
	item.Item.ProductType = productFromSolr.TypeID
	item.Item.Name = productFromSolr.Name
	// ToDo is item ID needed? Now updating same product it changes its id and makes additional request to Mysql for that id
	item.Item.ItemID = int(counter.GetAndIncreaseQuoteCounterInMySQL())

	UpdateCartInMySql(urlCartId, item.Item)
	response := helpers.Response{
		Code:   http.StatusOK,
		Result: item.Item}
	response.SendResponse(w)
}

func deleteFromUserCart(w http.ResponseWriter, r *http.Request) {
	urlCartId, err := helpers.GetCartIdFromUrl(r)
	helpers.CheckErr(err)
	var item Item
	err = json.NewDecoder(r.Body).Decode(&item)
	helpers.PanicErr(err)
	DeleteItemFromMySql(urlCartId, item)
	response := helpers.Response{
		Code:   http.StatusOK,
		Result: true}
	response.SendResponse(w)
}
