package cart

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-api/counter"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

// MySQL operations
func CreateCartInMySQL(userID string) string {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	quoteId := counter.GetAndIncreaseQuoteCounterInMySQL()

	quoteIdString := strconv.Itoa(int(quoteId))

	if userID == "" {
		res, err := db.Exec("INSERT INTO cart("+
			"quote_id, "+
			"created_at, "+
			"status) "+
			"VALUES(?, ?, ?)", quoteId, time.Now(), "Active")
		helpers.PanicErr(err)
		_, err = res.LastInsertId()
		helpers.PanicErr(err)

		return quoteIdString
	} else {
		res, err := db.Exec("INSERT INTO cart("+
			"quote_id, "+
			"created_at, "+
			"user_id, "+
			"status) "+
			"VALUES(?, ?, ?, ?)", quoteId, time.Now(), userID, "Active")
		helpers.PanicErr(err)
		_, err = res.LastInsertId()
		helpers.PanicErr(err)

		return quoteIdString
	}
}

func CheckIfUserHasACart(userId string) (cartId string, err error) {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)
	err = db.QueryRow("SELECT quote_id FROM cart WHERE status=? AND user_id=?",
		"Active", userId).
		Scan(&cartId)
	return cartId, err
}

func UpdateCartInMySql(cartId string, item Item) {
	jsonItem, err := json.Marshal(item)
	var itemArray []Item
	itemArray = append(itemArray, item)

	jsonItemArray, err := json.Marshal(itemArray)

	helpers.PanicErr(err)

	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	cartIdInt, err := strconv.Atoi(cartId)
	helpers.PanicErr(err)
	cartIdInt64 := int64(cartIdInt)

	query, err := db.Prepare("UPDATE cart SET items = IFNULL(JSON_ARRAY_APPEND(items, '$', CAST(? AS JSON)), CAST(? AS JSON)) where quote_id = ?;")

	helpers.PanicErr(err)

	_, err = query.Exec(jsonItem, jsonItemArray, cartIdInt64)
	helpers.CheckErr(err)

}

//func GetCartFromMySql(cartId string)  Cart {
//	cart := Cart{}
//	var cartItemsString sql.NullString
//
//	db, err := config.Conf.GetDb()
//	helpers.PanicErr(err)
//	err = db.QueryRow("SELECT id, created_at, quote_id, status, user_id, items FROM cart WHERE quote_id = ?", cartId).
//		Scan(&cart.CartId, &cart.CreatedAt, &cart.QuoteId, &cart.Status, &cart.UserId, &cartItemsString)
//
//	fmt.Println("cartItems: ", cart.Items)
//	if cartItemsString.Valid {
//		json.Unmarshal([]byte(cartItemsString.String), cart.Items)
//	} else {
//		cart.Items = make([]Item, 0)
//	}
//
//	if err != nil {// ToDo gets in to loop some times, needs testing
//		fmt.Println("ERROR IN GetCartFromMySql: ", err)
//		cartId = CreateCartInMySQL(cartId)
//		GetCartFromMySql(cartId)
//	}
//	return cart
//}

func newCart() *Cart {
	cart := Cart{}
	cart.Items = make([]Item, 0)
	return &cart
}
func GetCartFromMySql(cartId string) Cart {
	cartJson := MySqlCart{}
	cart := newCart()
	var cartItemsString sql.NullString

	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)
	err = db.QueryRow("SELECT id, created_at, quote_id, status, user_id, items FROM cart WHERE quote_id = ? AND status = ?", cartId, "Active").
		Scan(&cart.CartId, &cart.CreatedAt, &cart.QuoteId, &cart.Status, &cart.UserId, &cartItemsString)

	if cartItemsString.Valid {
		err = json.Unmarshal([]byte(cartItemsString.String), &cart.Items)
		helpers.CheckErr(err)
	}
	fmt.Println("cartItems: ", cart.Items)
	fmt.Println("cart JSON Items: ", cartJson.Items)
	if err != nil { // ToDo gets in to loop some times, needs testing
		fmt.Println("ERROR IN GetCartFromMySql: ", err)
		cartId = CreateCartInMySQL(cartId)
		GetCartFromMySql(cartId)
	}
	return *cart
}

func DeleteItemFromMySql(cartId string, item Item) {
	//jsonItem, err := json.Marshal(item)
	//db, err := config.Conf.GetDb()
	//helpers.PanicErr(err)
	cart := GetCartFromMySql(cartId)
	fmt.Println("cart: ", cart)
	for i := range cart.Items {
		if cart.Items[i].SKU == item.SKU {
			tempCart := RemoveItemFromSlice(cart.Items, i)
			cart.Items = tempCart
		}
	}
	itemsJson, err := json.Marshal(cart.Items)
	helpers.CheckErr(err)
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	_, err = db.Exec("Update cart set items=? where quote_id =?", itemsJson, cartId)
	helpers.CheckErr(err)
}

func RemoveItemFromSlice(s []Item, i int) []Item {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

func UpdateCartStatusInMySQL(cartId int64) {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)
	_, err = db.Exec("UPDATE cart set status=? where quote_id =?", "Inactive", cartId)
	helpers.PanicErr(err)

}
