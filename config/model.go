package config

import (
	sql "github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"net/http"
)

const (
	MySecret = "SenelisMegstaMociutesApvalumus"
)

var (
	Flags FlagSettings
	Conf  *Config

	db         *sql.DB
	dbUri      string
	driverName string

	//mongoDB *mongo.Database
)

type SolrConfig struct {
	Host string `yaml:"host"`
}

type EmailConfig struct {
	From         string `yaml:"from"`
	SmtpUsername string `yaml:"smtpUsername"`
	SmtpPassword string `yaml:"smtpPassword"`
	Name         string `yaml:"name"`
}

type (
	DbConfig struct {
		Server     string `yaml:"server"`
		DriverName string `yaml:"driverName"`
		DbName     string `yaml:"dbname,omitempty"`
		User       string `yaml:"user,omitempty"`
		Password   string `yaml:"password,omitempty"`
		Charset    string `yaml:"charset,omitempty"`
	}
	MongoDBConfig struct {
		Connection string `yaml:"connection"`
		DbName     string `yaml:"db_name"`
	}

	Config struct {
		Port       string        `yaml:"port"`
		AssetsPath string        `yaml:"assets"`
		LogFile    string        `yaml:"logFile"`
		Host       string        `yaml:"host"`
		Email      EmailConfig   `yaml:"email"`
		Db         DbConfig      `yaml:"db"`
		MongoDB    MongoDBConfig `yaml:"mongoDB"`
		Solr       SolrConfig    `yaml:"solr"`
		LogPath    string
		Log        *log.Logger
	}

	FlagSettings struct {
		Config     string
		AssetsPath string
		Assets     http.Dir
		Host       string
		Port       string
		LogFile    string
	}
)
