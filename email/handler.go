package email

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/auth"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func createMail(w http.ResponseWriter, r *http.Request) {

	recipient := "bauzys@gmail.com"
	subject := "Test subject from sway commerce"

	SendMail("Test mail body", recipient, subject)

	urlToken, err := helpers.GetTokenFromHeader(r)
	helpers.CheckErr(err)

	if helpers.HasToken(urlToken) {
		token := auth.ParseToken(urlToken)
		claims, err := auth.GetTokenClaims(token)
		helpers.CheckErr(err)
		log.Infof("Token hs claims: %v", claims)

		recipient := "bauzys@gmail.com"
		subject := "Test subject from sway commerce"

		SendMail("Test mail body", recipient, subject)

	} else {
		// user is not registered. operation should be canceled

		resp := map[string]int{
			"code": 401}

		helpers.WriteResultWithStatusCode(w, resp, http.StatusUnauthorized)
	}
}
