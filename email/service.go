package email

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/config"
	"github.com/pkg/errors"
	"gopkg.in/gomail.v2"
)

func SendMail(body, to, subject string) error {
	email := config.Conf.Email
	m := gomail.NewMessage()
	m.SetAddressHeader("From", email.From, email.Name)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	d := gomail.NewDialer("smtp.gmail.com", 587, email.SmtpUsername, email.SmtpPassword)

	if err := d.DialAndSend(m); err != nil {
		err = errors.Wrap(err, "error")
		return err
	}
	return nil
}

type MailConfig struct {
	userName   string
	password   string
	mailServer string
	mailPort   string
}

var mailConf *MailConfig
