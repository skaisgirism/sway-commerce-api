package email

import (
	"github.com/go-chi/chi"
	"net/http"
)

func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Post("/create", createMail)
	return r
}
