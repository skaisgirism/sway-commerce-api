package klarna

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterKlarna() http.Handler {
	r := chi.NewRouter()
	r.Post("/sessions", CreateKlarnaSession)
	r.Post("/order", CreateKlarnaOrder)
	r.Post("/accepted", HandleKlarnaPoints)
	return r
}
