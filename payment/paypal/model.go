package paypal

type (
	Transaction struct {
		Amount Amount `json:"amount"`
	}

	Amount struct {
		Total    float64 `json:"total"`
		Currency string `json:"currency"`
	}

	request struct {
		Transactions []Transaction `json:"transactions"`
		PaymentID    string        `json:"paymentID,omitempty"`
		PayerID      string        `json:"payerID,omitempty"`
	}

	Transaction2 struct {
		Amount Amount2 `json:"amount"`
	}

	Amount2 struct {
		Total    string `json:"total"`
		Currency string `json:"currency"`
	}

	request2 struct {
		Transactions []Transaction2 `json:"transactions"`
		PaymentID    string        `json:"paymentID,omitempty"`
		PayerID      string        `json:"payerID,omitempty"`
	}
)
