package paysera

import (
	"github.com/go-chi/chi"
	"net/http"
)

//Paysera routes
func RoutesPaysera() http.Handler {
	r := chi.NewRouter()
	r.Post("/sessions", CreatePayseraSession)
	r.Post("/order", CreatePayseraOrder)
	r.Post("/accepted", HandlePayseraPoints)
	return r
}



