package product

import (
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"encoding/json"
	"github.com/go-chi/chi"
	"net/http"
)

func InsertSimpleProductToDb(w http.ResponseWriter, r *http.Request) {
	var simpleProduct SimpleProductStruct
	err := json.NewDecoder(r.Body).Decode(&simpleProduct)
	helpers.PanicErr(err)
	simpleProduct.insertSimpleProductToDb()
}
func getProductBySkuHandler(w http.ResponseWriter, r *http.Request) {
	sku := chi.URLParam(r, "sku")
	product, err := getProductBySku(sku)
	if err != nil {
		helpers.Response{
			Code:  http.StatusInternalServerError,
			Error: err,
		}.SendResponse(w)
	}

	response := helpers.Response{
		Code:   http.StatusOK,
		Result: product}
	response.SendResponse(w)
}

func getSimpleProductFromDb(w http.ResponseWriter, r *http.Request) {
	productSKU, err := helpers.GetParameterFromUrl("sku", r)
	if err != nil {
		response := helpers.Response{
			Code:   http.StatusOK,
			Result: nil}
		response.SendResponse(w)
	}
	helpers.PanicErr(err)
	product := getSimpleProductFromDbBySku(productSKU)
	response := helpers.Response{
		Code:   http.StatusOK,
		Result: product}
	response.SendResponse(w)
}

func deleteProductFromDb(w http.ResponseWriter, r *http.Request) {
	productSKU, err := helpers.GetParameterFromUrl("sku", r)
	helpers.PanicErr(err)
	removeProductFromDbBySku(productSKU)
	response := helpers.Response{
		Code: http.StatusOK}
	response.SendResponse(w)
}

func updateSimpleProductInDb(w http.ResponseWriter, r *http.Request) {
	var simpleProduct SimpleProductStruct
	err := json.NewDecoder(r.Body).Decode(&simpleProduct)
	helpers.PanicErr(err)
	rowsAffected := simpleProduct.updateProductInDb()
	response := helpers.Response{
		Code:   http.StatusOK,
		Result: rowsAffected}
	response.SendResponse(w)
}
