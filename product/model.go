package product

import (
	c "bitbucket.org/sway-commerce/sway-commerce-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-api/helpers"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strings"
)

type Product struct {
	EntityID             string          `json:"entity_id" db:"entity_id"`
	Sku                  string          `json:"sku" db:"sku"`
	Name                 string          `json:"name" db:"name"`
	URL                  string          `json:"url"`
	Price                int64           `json:"price"`
	Qty                  int64           `json:"qty"`
	IsInStock            int64           `json:"is_in_stock" db:"is_in_stock"`
	Status               string          `json:"status"`
	CreatedAt            string          `json:"created_at" db:"created_at"`
	UpdatedAt            string          `json:"updated_at" db:"updated_at"`
	Description          string          `json:"description"`
	ShortDescription     interface{}     `json:"short_description" db:"short_description"`
	Brand                interface{}     `json:"brand"`
	Supplier             interface{}     `json:"supplier"`
	Color                interface{}     `json:"color"`
	Size                 interface{}     `json:"size"`
	Manufacturer         interface{}     `json:"manufacturer"`
	CountryOfManufacture interface{}     `json:"country_of_manufacture" db:"country_of_manufacture"`
	SpecialPrice         *string         `json:"special_price" db:"special_price"`
	GroupPrice           string          `json:"group_price" db:"group_price"`
	MediaGallery         string          `json:"media_gallery" db:"media_gallery"`
	Weight               interface{}     `json:"weight"`
	MetaTitle            *string         `json:"meta_title" db:"meta_title"`
	MetaKeywords         *string         `json:"meta_keywords" db:"meta_keywords"`
	MetaDescription      *string         `json:"meta_description" db:"meta_description"`
	TaxClass             string          `json:"tax_class" db:"tax_class"`
	TypeID               string          `json:"type_id" db:"type_id"`
	Visibility           string          `json:"visibility"`
	OtherAttributes      json.RawMessage `json:"other_attributes" db:"other_attributes"`
}

type (
	SimpleProductStruct struct {
		Index                          string        `json:"_index"`
		Type                           string        `json:"_type"`
		Score                          int           `json:"_score"`
		DocType                        string        `json:"doc_type"`
		ID                             int           `json:"id"`
		Sku                            string        `json:"sku"`
		Name                           string        `json:"name"`
		AttributeSetID                 int           `json:"attribute_set_id"`
		Price                          float64       `json:"price"`
		Status                         int           `json:"status"`
		Visibility                     int           `json:"visibility"`
		TypeID                         string        `json:"type_id"`
		CreatedAt                      string        `json:"created_at"`
		UpdatedAt                      string        `json:"updated_at"`
		CustomAttributes               interface{}   `json:"custom_attributes"`
		FinalPrice                     float64       `json:"final_price"`
		MaxPrice                       float64       `json:"max_price"`
		MaxRegularPrice                float64       `json:"max_regular_price"`
		MinimalRegularPrice            float64       `json:"minimal_regular_price"`
		MinimalPrice                   float64       `json:"minimal_price"`
		RegularPrice                   float64       `json:"regular_price"`
		ItemID                         int           `json:"item_id"`
		ProductID                      int           `json:"product_id"`
		StockID                        int           `json:"stock_id"`
		Qty                            int           `json:"qty"`
		IsInStock                      bool          `json:"is_in_stock"`
		IsQtyDecimal                   bool          `json:"is_qty_decimal"`
		ShowDefaultNotificationMessage bool          `json:"show_default_notification_message"`
		UseConfigMinQty                bool          `json:"use_config_min_qty"`
		MinQty                         int           `json:"min_qty"`
		UseConfigMinSaleQty            int           `json:"use_config_min_sale_qty"`
		MinSaleQty                     int           `json:"min_sale_qty"`
		UseConfigMaxSaleQty            bool          `json:"use_config_max_sale_qty"`
		MaxSaleQty                     int           `json:"max_sale_qty"`
		UseConfigBackorders            bool          `json:"use_config_backorders"`
		Backorders                     int           `json:"backorders"`
		UseConfigNotifyStockQty        bool          `json:"use_config_notify_stock_qty"`
		NotifyStockQty                 int           `json:"notify_stock_qty"`
		UseConfigQtyIncrements         bool          `json:"use_config_qty_increments"`
		QtyIncrements                  int           `json:"qty_increments"`
		UseConfigEnableQtyInc          bool          `json:"use_config_enable_qty_inc"`
		EnableQtyIncrements            bool          `json:"enable_qty_increments"`
		UseConfigManageStock           bool          `json:"use_config_manage_stock"`
		ManageStock                    bool          `json:"manage_stock"`
		LowStockDate                   interface{}   `json:"low_stock_date"`
		IsDecimalDivided               bool          `json:"is_decimal_divided"`
		StockStatusChangedAuto         int           `json:"stock_status_changed_auto"`
		Tsk                            int64         `json:"tsk"`
		Description                    string        `json:"description"`
		Image                          string        `json:"image"`
		SmallImage                     string        `json:"small_image"`
		Thumbnail                      string        `json:"thumbnail"`
		CategoryIds                    []int         `json:"category_ids"`
		OptionsContainer               string        `json:"options_container"`
		RequiredOptions                string        `json:"required_options"`
		HasOptions                     string        `json:"has_options"`
		URLKey                         string        `json:"url_key"`
		TaxClassID                     int           `json:"tax_class_id"`
		Activity                       string        `json:"activity,omitempty"`
		Material                       string        `json:"material,omitempty"`
		Gender                         string        `json:"gender"`
		CategoryGear                   string        `json:"category_gear"`
		ErinRecommends                 string        `json:"erin_recommends"`
		New                            string        `json:"new"`
		Sale                           string        `json:"sale"`
		ChildDocuments                 []interface{} `json:"_childDocuments_,omitempty"`
	}

	ConfigurableProductStruct struct {
		Index                          string        `json:"_index"`
		Type                           string        `json:"_type"`
		Score                          int           `json:"_score"`
		DocType                        string        `json:"doc_type"`
		ID                             string        `json:"id"`
		Sku                            string        `json:"sku"`
		Name                           string        `json:"name"`
		AttributeSetID                 int           `json:"attribute_set_id"`
		Price                          float64       `json:"price"`
		Status                         int           `json:"status"`
		Visibility                     int           `json:"visibility"`
		TypeID                         string        `json:"type_id"`
		CreatedAt                      string        `json:"created_at"`
		UpdatedAt                      string        `json:"updated_at"`
		CustomAttributes               interface{}   `json:"custom_attributes"`
		FinalPrice                     float64       `json:"final_price"`
		MaxPrice                       float64       `json:"max_price"`
		MaxRegularPrice                float64       `json:"max_regular_price"`
		MinimalRegularPrice            float64       `json:"minimal_regular_price"`
		MinimalPrice                   float64       `json:"minimal_price"`
		RegularPrice                   float64       `json:"regular_price"`
		ItemID                         int           `json:"item_id"`
		ProductID                      int           `json:"product_id"`
		StockID                        int           `json:"stock_id"`
		Qty                            int           `json:"qty"`
		IsInStock                      bool          `json:"is_in_stock"`
		IsQtyDecimal                   bool          `json:"is_qty_decimal"`
		ShowDefaultNotificationMessage bool          `json:"show_default_notification_message"`
		UseConfigMinQty                bool          `json:"use_config_min_qty"`
		MinQty                         int           `json:"min_qty"`
		UseConfigMinSaleQty            int           `json:"use_config_min_sale_qty"`
		MinSaleQty                     int           `json:"min_sale_qty"`
		UseConfigMaxSaleQty            bool          `json:"use_config_max_sale_qty"`
		MaxSaleQty                     int           `json:"max_sale_qty"`
		UseConfigBackorders            bool          `json:"use_config_backorders"`
		Backorders                     int           `json:"backorders"`
		UseConfigNotifyStockQty        bool          `json:"use_config_notify_stock_qty"`
		NotifyStockQty                 int           `json:"notify_stock_qty"`
		UseConfigQtyIncrements         bool          `json:"use_config_qty_increments"`
		QtyIncrements                  int           `json:"qty_increments"`
		UseConfigEnableQtyInc          bool          `json:"use_config_enable_qty_inc"`
		EnableQtyIncrements            bool          `json:"enable_qty_increments"`
		UseConfigManageStock           bool          `json:"use_config_manage_stock"`
		ManageStock                    bool          `json:"manage_stock"`
		LowStockDate                   interface{}   `json:"low_stock_date"`
		IsDecimalDivided               bool          `json:"is_decimal_divided"`
		StockStatusChangedAuto         int           `json:"stock_status_changed_auto"`
		ColorOptions                   []int         `json:"color_options"`
		SizeOptions                    []int         `json:"size_options"`
		Tsk                            int64         `json:"tsk"`
		Description                    string        `json:"description"`
		Image                          string        `json:"image"`
		SmallImage                     string        `json:"small_image"`
		Thumbnail                      string        `json:"thumbnail"`
		CategoryIds                    []int         `json:"category_ids"`
		OptionsContainer               string        `json:"options_container"`
		RequiredOptions                []int         `json:"required_options"`
		HasOptions                     []int         `json:"has_options"`
		URLKey                         string        `json:"url_key"`
		MsrpDisplayActualPriceType     string        `json:"msrp_display_actual_price_type"`
		TaxClassID                     int           `json:"tax_class_id,omitempty"`
		Material                       string        `json:"material"`
		EcoCollection                  string        `json:"eco_collection"`
		PerformanceFabric              string        `json:"performance_fabric"`
		ErinRecommends                 string        `json:"erin_recommends"`
		New                            string        `json:"new"`
		Sale                           string        `json:"sale"`
		Pattern                        string        `json:"pattern"`
		Climate                        []string      `json:"climate"`
		ChildDocuments                 []interface{} `json:"_childDocuments_,omitempty"`
	}

	solrResponse struct {
		ResponseHeader struct {
			Status int `json:"status"`
			QTime  int `json:"QTime"`
			Params struct {
				JSON string `json:"json"`
			}
		} `json:"responseHeader"`
		Response struct {
			NumFound int                         `json:"numFound"`
			Start    int                         `json:"start"`
			Docs     []ConfigurableProductStruct `json:"docs"`
		} `json:"response"`
	}
)

var productFields = []string{"entity_id", "sku", "name", "url", "price", "qty", "is_in_stock", "status",
	"created_at", "updated_at", "description", "brand", "supplier", "color", "size",
	"manufacturer", "country_of_manufacture", "special_price", "group_price",
	"media_gallery", "weight", "meta_title", "meta_keywords", "meta_description",
	"tax_class", "type_id", "visibility", "other_attributes"}

var productFieldsString = strings.Join(productFields, ",")

var ErrNoData = errors.New("product: product not found")

func deleteProduct(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	res, err := db.Exec("DELETE p FROM products p WHERE p.entity_id=?", entityID)
	fmt.Println(res)
	helpers.CheckErrHttp(err, w)
}

func getProductBySku(sku string) (*Product, error) {
	db, err := c.Conf.GetDb()
	if err != nil {
		return nil, err
	}

	var product Product
	err = db.Get(&product, db.Rebind("SELECT "+productFieldsString+" FROM products p WHERE sku=?"), sku)
	if err == sql.ErrNoRows {
		return nil, ErrNoData
	}
	return &product, err
}
